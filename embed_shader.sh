#!/bin/bash
# $1 = variable to be embedded
# $2 = file to be embedded
# $3 = file to embed to

printf '#pragma once\nconstexpr const char *%s = R"***(\n%s)***";\n' $1 "$(cat $2 | sed 's/\n/\\n/')" > $3
