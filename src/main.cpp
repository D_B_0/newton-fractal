#include <algorithm>
#include <complex>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <unordered_map>

#include "glad/glad.h"
#include <GLFW/glfw3.h>

#include "polynomial.hpp"
#include "shaders.hpp"

static unsigned int WIDTH = 800;
static unsigned int HEIGHT = 600;
static double SCROLL_DIR = 0;
static bool SCROLLED = false;
static bool RERENDER = true;

#ifndef __GNUC__
#pragma region INITIALIZAION_UTILITY
#endif
GLFWwindow *initializeGLFW()
{
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  // glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  GLFWwindow *window = glfwCreateWindow(
      WIDTH, HEIGHT, "Interactive Newton Fractal", NULL, NULL);
  if (window == NULL)
  {
    std::cerr << "Failed to create GLFW window\n";
    glfwTerminate();
    exit(-1);
  }
  return window;
}

void initializeGLAD(GLFWwindow *window)
{
  (void)window;
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
  {
    std::cerr << "Failed to initialize GLAD" << std::endl;
    exit(-1);
  }
}
#ifndef __GNUC__
#pragma endregion
#endif

#ifndef __GNUC__
#pragma region UNIFORM_UTILITY
#endif
int getUniformLocation(unsigned int &program, std::string uniform_name)
{
  static std::unordered_map<std::string, int> map;
  glUseProgram(program);
  int uniformLocation;
  try
  {
    uniformLocation = map.at(uniform_name);
  }
  catch (const std::out_of_range &)
  {
    uniformLocation = glGetUniformLocation(program, uniform_name.c_str());
    map[uniform_name] = uniformLocation;
    if (uniformLocation == -1)
    {
      std::cerr << "Unable to find `" << uniform_name << "` uniform.\n";
    }
  }
  return uniformLocation;
}

bool setUniform1f(unsigned int &program, std::string uniform_name, float value)
{
  int uniformLocation = getUniformLocation(program, uniform_name);
  if (uniformLocation == -1)
    return false;
  glUseProgram(program);
  glUniform1f(uniformLocation, value);
  return true;
}

bool setUniform2f(unsigned int &program, std::string uniform_name,
                  float value1, float value2)
{
  int uniformLocation = getUniformLocation(program, uniform_name);
  if (uniformLocation == -1)
    return false;
  glUseProgram(program);
  glUniform2f(uniformLocation, value1, value2);
  return true;
}
#ifndef __GNUC__
#pragma endregion
#endif

complex screenToWorld(double screenx, double screeny, complex origin,
                      double scale)
{
  complex worldPos;
  float res = static_cast<float>(std::max(WIDTH, HEIGHT))
              / static_cast<float>(std::min(WIDTH, HEIGHT));
  screenx -= static_cast<double>(WIDTH) / 2;
  screeny -= static_cast<double>(HEIGHT) / 2;
  worldPos = complex{ screenx, -screeny }
             / (static_cast<double>(std::min(WIDTH, HEIGHT)) / 2);
  worldPos *= scale;
  if (WIDTH > HEIGHT)
    worldPos -= complex{ origin.real() * res, origin.imag() };
  else
    worldPos -= complex{ origin.real(), origin.imag() * res };
  return worldPos;
}
#define ROOT_PICKUP_RADIUS 0.02

int main()
{
  GLFWwindow *window = initializeGLFW();
  glfwMakeContextCurrent(window);
  initializeGLAD(window);
  glViewport(0, 0, WIDTH, HEIGHT);
  glfwSetFramebufferSizeCallback(
      window, [](GLFWwindow *window, int width, int height) {
        (void)window;
        // std::cout << "Resizing window. width: " << width
        //           << ", height: " << height << "\n";
        glViewport(0, 0, width, height);
        RERENDER = true;
        WIDTH = width;
        HEIGHT = height;
      });
  glfwSetScrollCallback(
      window, [](GLFWwindow *window, double xoffset, double yoffset) {
        (void)window;
        (void)xoffset;
        // std::cout << "Scrolled. xoffset: " << xoffset
        //           << " yoffset: " << yoffset << "\n";
        SCROLL_DIR = yoffset;
        RERENDER = true;
        SCROLLED = true;
      });

  // clang-format off
  float vertices[] = {
       1.0f,  1.0f, 0.0f,  // top right
       1.0f, -1.0f, 0.0f,  // bottom right
      -1.0f, -1.0f, 0.0f,  // bottom left
      -1.0f,  1.0f, 0.0f   // top left 
  };
  unsigned int indices[] = {
      0, 1, 3,   // first triangle
      1, 2, 3    // second triangle
  };
  // clang-format on

#ifndef __GNUC__
#pragma region SHADER_STUFF
#endif
  // std::cout << "Vertex shader:\n" << vertexShaderSource << "\n";
  unsigned int vertexShader;
  vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &shader_vert_source, NULL);
  glCompileShader(vertexShader);

  int success;
  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);

  if (!success)
  {
    char infoLog[512];
    glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
    std::cerr << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n"
              << infoLog << "\n";
    exit(1);
  }
  else
  {
    std::cout << "Vertex shader compiled successfully\n";
  }

  // std::cout << "Fragment shader:\n" << fragmentShaderSource << "\n";
  unsigned int fragmentShader;
  fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &shader_frag_source, NULL);
  glCompileShader(fragmentShader);

  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);

  if (!success)
  {
    char infoLog[512];
    glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
    std::cerr << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n"
              << infoLog << "\n";
    exit(1);
  }
  else
  {
    std::cout << "Fragment shader compiled successfully\n";
  }

  // Shader program
  unsigned int shaderProgram;
  shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);

  glLinkProgram(shaderProgram);

  glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);

  if (!success)
  {
    char infoLog[512];
    glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
    std::cerr << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << "\n";
    exit(1);
  }
  else
  {
    std::cout << "Program linked successfully\n";
  }

  glUseProgram(shaderProgram);

  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);
#ifndef __GNUC__
#pragma endregion
#endif

  unsigned int VAO;
  glGenVertexArrays(1, &VAO);

  unsigned int VBO;
  glGenBuffers(1, &VBO);

  unsigned int EBO;
  glGenBuffers(1, &EBO);

  // 1. bind Vertex Array Object
  glBindVertexArray(VAO);
  // 2. copy our vertices array in a vertex buffer for OpenGL to use
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  // 3. copy our index array in a element buffer for OpenGL to use
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
               GL_STATIC_DRAW);
  // 4. then set the vertex attributes pointers
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
                        (void *)0);
  glEnableVertexAttribArray(0);

  setUniform1f(shaderProgram, "u_Resolution", 1);

  // clang-format off
  cArr5 roots = {
    complex{  1.0f,  0.0f },
    complex{ -0.6f,  0.6f },
    complex{ -0.6f, -0.6f },
    complex{  0.0f,  0.8f },
    complex{  0.0f, -0.8f },
  };
  // clang-format on
  std::array<bool, 5> dragging_root = { false };

  cArr6 poly_coeff = coeff_from_roots(roots);
  cArr5 derivative_coeff = derivative(poly_coeff);

  complex origin{ 0.0, 0.0 };

  bool dragging_world = false;
  complex dragging_world_start{};

  double scale = 1;
  const double scroll_speed = 0.9f;

  while (!glfwWindowShouldClose(window))
  {
    float res = static_cast<float>(std::max(WIDTH, HEIGHT))
                / static_cast<float>(std::min(WIDTH, HEIGHT));
    // input
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
      glfwSetWindowShouldClose(window, true);

    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    complex mousePos = screenToWorld(xpos, ypos, origin, scale);
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS)
    {
      RERENDER = true;
      if (!dragging_world)
      {
        dragging_world = true;
        dragging_world_start = mousePos;
      }
      origin -= dragging_world_start - mousePos;
    }
    else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE)
             == GLFW_RELEASE)
    {
      dragging_world = false;
    }

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
    {
      RERENDER = true;
      if (!std::any_of(dragging_root.begin(), dragging_root.end(),
                       [](bool b) { return b; }))
      {
        for (size_t i = 0; i < roots.size(); i++)
        {
          if (std::abs(roots[i] - mousePos) / scale < ROOT_PICKUP_RADIUS)
          {
            dragging_root[i] = true;
            break;
          }
        }
      }
    }
    else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)
             == GLFW_RELEASE)
    {
      dragging_root.fill(false);
    }

    bool roots_updated = false;
    for (size_t i = 0; i < dragging_root.size(); i += 1)
    {
      if (dragging_root[i])
      {
        roots[i] = mousePos;
        roots_updated = true;
        break;
      }
    }

    if (SCROLL_DIR != 0 && SCROLLED)
    {
      scale *= std::pow(scroll_speed, SCROLL_DIR);
      SCROLLED = false;
    }

    if (roots_updated)
    {
      poly_coeff = coeff_from_roots(roots);
      derivative_coeff = derivative(poly_coeff);
    }

    // uniform updating
    if (RERENDER)
    {
      glUseProgram(shaderProgram);
      glBindVertexArray(VAO);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

      setUniform1f(shaderProgram, "u_Time", glfwGetTime());
      setUniform1f(shaderProgram, "u_Width", WIDTH);
      setUniform1f(shaderProgram, "u_Height", HEIGHT);
      setUniform1f(shaderProgram, "u_Resolution", res);
      setUniform2f(shaderProgram, "u_Origin", origin.real(), origin.imag());
      setUniform1f(shaderProgram, "u_Scale", scale);

      for (size_t i = 0; i < roots.size(); i++)
      {
        setUniform2f(shaderProgram,
                     std::string{ "u_Root" } + std::to_string(i),
                     roots[i].real(), roots[i].imag());
      }

      for (size_t i = 0; i < poly_coeff.size(); i++)
      {
        setUniform2f(shaderProgram, std::string{ "u_f_" } + std::to_string(i),
                     poly_coeff[i].real(), poly_coeff[i].imag());
      }

      for (size_t i = 0; i < derivative_coeff.size(); i++)
      {
        setUniform2f(shaderProgram, std::string{ "u_df_" } + std::to_string(i),
                     derivative_coeff[i].real(), derivative_coeff[i].imag());
      }

      // rendering
      glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
      glClear(GL_COLOR_BUFFER_BIT);

      glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

      // call events and buffers
      glfwSwapBuffers(window);
      RERENDER = false;
    }
    glfwPollEvents();
  }

  glfwTerminate();
  return 0;
}