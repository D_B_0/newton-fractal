#include <array>
#include <complex>

typedef std::complex<double> complex;
typedef std::array<complex, 5> cArr5;
typedef std::array<complex, 6> cArr6;

cArr6 coeff_from_roots(const cArr5 &r);
cArr5 derivative(const cArr6 &coeff);
