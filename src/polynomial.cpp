#include "polynomial.hpp"
#include <iostream>

cArr6 coeff_from_roots(const cArr5 &r)
{
  return {
    -(r[0] * r[1] * r[2] * r[3] * r[4]),
    (r[0] * r[1] * r[2] * r[3]) + (r[0] * r[1] * r[2] * r[4])
        + (r[0] * r[1] * r[3] * r[4]) + (r[0] * r[2] * r[3] * r[4])
        + (r[1] * r[2] * r[3] * r[4]),
    -((r[0] * r[1] * r[2]) + (r[0] * r[1] * r[3]) + (r[0] * r[1] * r[4])
      + (r[0] * r[2] * r[3]) + (r[0] * r[2] * r[4]) + (r[0] * r[3] * r[4])
      + (r[1] * r[2] * r[3]) + (r[1] * r[2] * r[4]) + (r[1] * r[3] * r[4])
      + (r[2] * r[3] * r[4])),
    (r[0] * r[1] + r[0] * r[2] + r[0] * r[3] + r[0] * r[4] + r[1] * r[2]
     + r[1] * r[3] + r[1] * r[4] + r[2] * r[3] + r[2] * r[4] + r[3] * r[4]),
    -(r[0] + r[1] + r[2] + r[3] + r[4]),
    1
  };
}

cArr5 derivative(const cArr6 &coeff)
{
  return {
    coeff[1],
    coeff[2] * complex{ 2 },
    coeff[3] * complex{ 3 },
    coeff[4] * complex{ 4 },
    coeff[5] * complex{ 5 },
  };
}
