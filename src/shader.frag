#version 330 core
precision highp float;
#define PI 3.1415926535897932384626433832795

out vec3 FragColor;

#pragma region UNIFORMS
uniform float u_Time;
uniform float u_Resolution;
uniform float u_Width;
uniform float u_Height;
uniform vec2  u_Origin;
uniform float u_Scale;

uniform vec2 u_Root0;
uniform vec2 u_Root1;
uniform vec2 u_Root2;
uniform vec2 u_Root3;
uniform vec2 u_Root4;

uniform vec2 u_f_0;
uniform vec2 u_f_1;
uniform vec2 u_f_2;
uniform vec2 u_f_3;
uniform vec2 u_f_4;
uniform vec2 u_f_5;

uniform vec2 u_df_0;
uniform vec2 u_df_1;
uniform vec2 u_df_2;
uniform vec2 u_df_3;
uniform vec2 u_df_4;
#pragma endregion

/*
 * GLSL HSV to RGB conversion. Useful for many effects and shader debugging.
 *
 * Copyright (c) 2012 Corey Tabaka
 *
 * Hue is in the range [0.0, 1.0] instead of degrees or radians.
 */

vec3 hsv_to_rgb(float h, float s, float v)
{
  float c = v * s;
  h = mod((h * 6.0), 6.0);
  float x = c * (1.0 - abs(mod(h, 2.0) - 1.0));
  vec3 color;

  if (0.0 <= h && h < 1.0) {
    color = vec3(c, x, 0.0);
  } else if (1.0 <= h && h < 2.0) {
    color = vec3(x, c, 0.0);
  } else if (2.0 <= h && h < 3.0) {
    color = vec3(0.0, c, x);
  } else if (3.0 <= h && h < 4.0) {
    color = vec3(0.0, x, c);
  } else if (4.0 <= h && h < 5.0) {
    color = vec3(x, 0.0, c);
  } else if (5.0 <= h && h < 6.0) {
    color = vec3(c, 0.0, x);
  } else {
    color = vec3(0.0, 0.0, 0.0);
  }

  color.rgb += v - c;

  return color;
}

vec2 cx_mult(vec2 z, vec2 w)
{
  // (ac−bd) + (ad+bc)i
  return vec2(z.x * w.x - z.y * w.y, z.x * w.y + z.y * w.x);
}

vec2 cx_div(vec2 z, vec2 w)
{
  float a = z.x;
  float b = z.y;
  float c = w.x;
  float d = w.y;
  return vec2((a * c + b * d) / (c * c + d * d), (b * c - a * d)/(c * c + d * d));
}

vec2 cx_pow(vec2 z, int x)
{
  if (x <= 0)
    return vec2(1.0f, 0.0f);
  vec2 w = z;
  for(int i = 1; i < x; i++) {
    w = cx_mult(w, z);
  }
  return w;
}

vec2 f(vec2 z)
{
  return cx_mult(u_f_5, cx_pow(z, 5)) +
         cx_mult(u_f_4, cx_pow(z, 4)) +
         cx_mult(u_f_3, cx_pow(z, 3)) +
         cx_mult(u_f_2, cx_pow(z, 2)) +
         cx_mult(u_f_1, z) +
         u_f_0;
}

vec2 df(vec2 z)
{
  return cx_mult(u_df_4, cx_pow(z, 4)) +
         cx_mult(u_df_3, cx_pow(z, 3)) +
         cx_mult(u_df_2, cx_pow(z, 2)) +
         cx_mult(u_df_1, z) +
         u_df_0;
}

#pragma region CONSTANTS
#define ROOT_POINT_OUTER_RADIUS 0.02f
#define ROOT_POINT_INNER_RADIUS ROOT_POINT_OUTER_RADIUS - 0.004f
#define ROOT0_COLOR vec3(0.811f, 0.886f, 0.952f)
#define ROOT1_COLOR vec3(0.835f, 0.650f, 0.741f)
#define ROOT2_COLOR vec3(0.435f, 0.658f, 0.862f)
#define ROOT3_COLOR vec3(0.403f, 0.305f, 0.654f)
#define ROOT4_COLOR vec3(0.576f, 0.768f, 0.490f)
#pragma endregion

vec3 color_from_complex(vec2 z)
{
  return hsv_to_rgb(atan(z.y, -z.x)/(2*PI), 1, 1);
}

void main()
{
#pragma region UV_MANIPULATION
  vec2 uv = ((gl_FragCoord.xy + vec2(.5)) - (vec2(u_Width, u_Height) / 2)) / (vec2(u_Width, u_Height) / 2) * u_Scale - u_Origin;
  if (u_Width > u_Height)
    uv.x *= u_Resolution;
  else
    uv.y *= u_Resolution;
#pragma endregion
#pragma region DRAW_ROOTS
  if (length(uv - u_Root0) / u_Scale < ROOT_POINT_OUTER_RADIUS)
  {
    if (length(uv - u_Root0) / u_Scale < ROOT_POINT_INNER_RADIUS)
      FragColor = vec3(ROOT0_COLOR);
    else
      FragColor = vec3(0.0f);
    return;
  }
  else if (length(uv - u_Root1) / u_Scale < ROOT_POINT_OUTER_RADIUS)
  {
    if (length(uv - u_Root1) / u_Scale < ROOT_POINT_INNER_RADIUS)
      FragColor = vec3(ROOT1_COLOR);
    else
      FragColor = vec3(0.0f);
    return;
  }
  else if (length(uv - u_Root2) / u_Scale < ROOT_POINT_OUTER_RADIUS)
  {
    if (length(uv - u_Root2) / u_Scale < ROOT_POINT_INNER_RADIUS)
      FragColor = vec3(ROOT2_COLOR);
    else
      FragColor = vec3(0.0f);
    return;
  }
  else if (length(uv - u_Root3) / u_Scale < ROOT_POINT_OUTER_RADIUS)
  {
    if (length(uv - u_Root3) / u_Scale < ROOT_POINT_INNER_RADIUS)
      FragColor = vec3(ROOT3_COLOR);
    else
      FragColor = vec3(0.0f);
    return;
  }
  else if (length(uv - u_Root4) / u_Scale < ROOT_POINT_OUTER_RADIUS)
  {
    if (length(uv - u_Root4) / u_Scale < ROOT_POINT_INNER_RADIUS)
      FragColor = vec3(ROOT4_COLOR);
    else
      FragColor = vec3(0.0f);
    return;
  }
#pragma endregion DRAW_ROOTS
  vec2 z = uv;
  int i = 0;
  for (; i < 1000; i++) {
    vec2 delta = -cx_div(f(z), df(z));
    z = z + delta;
    if (length(delta) < 0.000001) {
      break;
    }
  }
  float color_scale = 1.0/log(float(i+1));

  float root0_dist = length(u_Root0 - z);
  float root1_dist = length(u_Root1 - z);
  float root2_dist = length(u_Root2 - z);
  float root3_dist = length(u_Root3 - z);
  float root4_dist = length(u_Root4 - z);

  float min_dist = min(root0_dist, min(root1_dist, min(root2_dist, min(root3_dist, root4_dist))));

  if (min_dist == root0_dist)
    FragColor = ROOT0_COLOR * color_scale;
  if (min_dist == root1_dist)
    FragColor = ROOT1_COLOR * color_scale;
  if (min_dist == root2_dist)
    FragColor = ROOT2_COLOR * color_scale;
  if (min_dist == root3_dist)
    FragColor = ROOT3_COLOR * color_scale;
  if (min_dist == root4_dist)
    FragColor = ROOT4_COLOR * color_scale;
}
