# Newton's Fractal

![](./screencap.gif)

This is a c++ application wich uses opengl and glfw to make a real time interactive rendering of [Newton's fractal](https://en.wikipedia.org/wiki/Newton_fractal)

This project was inspired py [3blue1brown](https://www.youtube.com/c/3blue1brown)'s [video](https://www.youtube.com/watch?v=-RdOwhmqP5s) on the subject