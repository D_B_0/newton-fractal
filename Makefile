CXX							= g++
CXX_WIN					= i686-w64-mingw32-g++
CXXFLAGS				= -Wall -Wextra -Werror
LDFLAGS					= -lGL -lglfw -lX11 -lXrandr -lXi -ldl
LDFLAGS_WIN			= -lglfw3 -lopengl32 -lmingw32 -lgdi32 -luser32 -lkernel32 -lssp
VENDOR_DIR			= ./vendor
VENDOR_SRC			= $(VENDOR_DIR)/src
VENDOR_INCLUDE	= $(VENDOR_DIR)/include
VENDOR_LIB			= $(VENDOR_DIR)/lib
INCLUDE					= -I$(VENDOR_INCLUDE)
SRC_DIR					= src
SRC_FILES				= $(wildcard $(SRC_DIR)/*.cpp)
HEADER_FILES		= $(wildcard $(SRC_DIR)/*.hpp)
TARGET					= newton

.PHONY: all build run win shaders info

all: build win

$(TARGET): $(SRC_FILES) $(HEADER_FILES)
	$(CXX) $(CXXFLAGS) -o $@ $(SRC_FILES) $(VENDOR_SRC)/glad.c $(INCLUDE) $(LDFLAGS)

$(TARGET).exe: $(SRC_FILES) $(HEADER_FILES)
	$(CXX_WIN) $(CXXFLAGS) -o $@ $(SRC_FILES) $(VENDOR_SRC)/glad.c $(INCLUDE) $(LDFLAGS_WIN)

$(SRC_DIR)/vert.hpp: $(SRC_DIR)/shader.vert
	./embed_shader.sh shader_vert_source $< $@

$(SRC_DIR)/frag.hpp: $(SRC_DIR)/shader.frag
	./embed_shader.sh shader_frag_source $< $@

shaders: $(SRC_DIR)/vert.hpp $(SRC_DIR)/frag.hpp

build: shaders $(TARGET)
	@echo $(CXXFLAGS) $(LDFLAGS) $(INCLUDE) | sed "s/ /\n/g" > compile_flags.txt

run: build
	./$(TARGET)

win: shaders $(TARGET).exe

info:
	@echo "TARGET		=" $(TARGET)
	@echo "WINDOWS TARGET	=" $(SRC_FILES)
	@echo "SRC_FILES	=" $(SRC_FILES)
	@echo "HEADER_FILES	=" $(HEADER_FILES)
